@extends ('index')


@section ('title') Detalles de pago de Venta @stop

@section ('content')

<div class="panel panel-default" ng-app='pagoApp'>
<div class="panel-heading" >
    <h1> Detalle de pago </h1>
        {{ HTML::link('pago', 'Todos', array('class' => 'glyphicon glyphicon-list'))}}
  </div>
  <div class="panel-body" ng-controller = 'total'>
  @include ('errors', array('errors' => $errors)) 

{{ Form::model($pago, $form_data, array('role' => 'form')) }}
       
        {{ Form::hidden('idVenta', null, array('placeholder' => 'ID', 'class' => 'form-control', 'readonly'=>'true')) }}
    
  <div class="row">
     
      <div class="form-group col-md-2">        
        {{ Form::label('pagoTotal', 'Cantidad.') }}
        <div class="input-group">
          <span class="input-group-addon">$</span>
          {{ Form::text('pagoTotal', $pago->venta->total(), array('placeholder' => 'Cantidad Total', 'ng-model' => 'total' ,'class' => 'form-control', 'readonly')) }}
        </div>
      </div>
      <div class="form-group col-md-2">        
        {{ Form::label('prima', 'Prima') }}
        <div class="input-group">
          <span class="input-group-addon">$</span>
          {{ Form::text('prima', null , array('placeholder' => 'Prima', 'ng-model' => 'prima' ,'ng-change' => 'calcular()' ,'class' => 'form-control')) }}
        </div>
      </div>
      <div class="form-group col-md-2">        
        {{ Form::label('pagar', 'Cuota') }}
        <div class="input-group">
          <span class="input-group-addon">$</span>
          {{ Form::text('pagar', null , array('readOnly', 'placeholder' => 'Cuota a pagar', 'ng-model' => 'pagar' ,'ng-change' => 'calcular()' ,'class' => 'form-control')) }}
        </div>
      </div>
  </div>
  <div class="row">
      <div class="form-group col-md-2">        
        {{ Form::label('numeroCuotas', 'Numero de Cuotas.') }}
        {{ Form::text('numeroCuotas', '1', array('placeholder' => 'Numero de Cuotas', 'ng-model' => 'cuota' ,'class' => 'form-control cuotas')) }}
      </div>
      <div class="form-group col-md-2">
        {{ Form::label('detallePago', 'Tiempo de Pago') }}
        {{ Form::select('detallePago', array('Mensual' => 'Mensual', 'Anual' => 'Anual'), 'Contado', array('class'=>'form-control')); }}
      </div>
     <div style="color:white"> @{{ pagar = ((total - prima) / cuota);}} </div>
  </div>

    {{ Form::button($action, array('type' => 'submit', 'class' => 'btn btn-success')) }}
  {{Form::close()}}
  </div>
  @{{ total = <?php echo $pago->venta->total() ?>; cuota = '5'; prima = <?php echo $pago->prima ?>; ''}}

</div>  
 

<script>
  

var pagoApp = angular.module('pagoApp', []);

pagoApp.controller('total', function($scope)
{
  $scope.calcular = function(){
  }
});



  
</script>
@stop