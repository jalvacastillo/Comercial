@extends ('index')

@section ('title') Lista de pagos a crédito @stop

@section ('content')

<div class="panel panel-default">
  <div class="panel-heading">
    <p>
        <h1>Lista de pagos a crédito</h1>
    </p>
  </div>
  <div class="panel-body">
 
    {{ $clientes->links()}} 

  <table class="table table-striped table-condensed table-hover">
    <thead>
    <tr>
        <th>Fecha</th>
        <th>Cliente</th>
        <th>Venta</th>
        <th>Total</th>
        <th>Prima</th>
        <th>Abono</th>
        <th>Saldo</th>
        <th> Cuota </th>
        <th>Cuotas</th>
        <th>Tiempo pago</th>
        <th>Prox. pago</th>
        <th>Opciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($clientes as $cliente)
        @foreach ($cliente->compras as $pago)

             <?php 
            //Calculos para mostrarse en la tabla.
               $hoy = date("Y-m-d");  
                $pagos = $pago->pagos()->sum('cantidad'); 
                $total = $pago->venta->total(); 
                $saldo = $total  - ($pagos + $pago->prima);

                $pagosRealizados = $pago->pagos()->count();
                $date = date_create($pago->created_at);
                date_add($date, date_interval_create_from_date_string($pagosRealizados + 1 . ($pago->detallePago == 'Anual' ? "years" : "months")));
                $fecha = date_format($date, 'Y-m-d');
            
            ?>
            
        <tr class="{{ ($saldo > 0 ? ($fecha < $hoy ? 'danger':'warning') : 'success') }}">
            <td>{{ date_format($pago->created_at, 'd-m-Y') }}</td>
            <td>{{ $cliente->nombre  }}</td>
            <td>{{ $pago->idVenta }}</td>
            <td> $ {{ $total }}</td>
            <td> $ {{ $pago->prima }}</td>
            <td> $ {{ $pagos }}</td>
            <td> $ {{ $saldo}}</td>
            <td> $ {{ ($total - $pago->prima) / $pago->numeroCuotas }}</td>
            <td> {{ $pagosRealizados }} / {{ $pago->numeroCuotas }}</td>
            <td>{{ $pago->detallePago }}</td>
            <td> {{ date_format($date, 'd-m-Y') }}</td>
            <td>
                <a href="{{ route('pago.edit', $pago->id) }}" class="btn btn-primary glyphicon glyphicon-edit"> </a>
                <a href="{{ route('detallepago.edit', $pago->id) }}" class="btn btn-primary glyphicon glyphicon-search"> </a>
            </td>
        </tr>
        @endforeach
    @endforeach
    </tbody>
  </table>
</div>
</div>

{{--usado para eliminar usuario --}}
{{ Form::open(array('route' => array('pago.destroy', 'CLIENTE_ID'), 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete')) }}
{{ Form::close() }}
@stop




