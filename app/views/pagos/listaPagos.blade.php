@extends ('index')

@section ('title') Lista de pagos a crédito @stop

@section ('content')

<div class="panel panel-default">
  <div class="panel-heading">
    <p>
        <h1>
        	Pagos realizados
        </h1>
    </p>
  </div>
  <div class="panel-body">
 	{{ $pagos->links()}} 
	  <table class="table table-striped table-condensed table-hover">
	    <thead>
	    <tr>
	        <th>Fecha</th>
	        <th>Empresario</th>
	        <th>Cantidad</th>
	        <th>Descuento</th>
	        <th>Total</th>
	        <th>Opciones</th>
	    </tr>
	    </thead>
	    <tbody>
	    @foreach ($pagos as $pago)
	    <tr>
	        <td>{{ $pago->fecha }}</td>
	        <td> @if($pago->credito)
	        		{{ $pago->credito->venta->cliente->nombre }}
	        	@endif	
	        </td>
	        <td>$ {{ $pago->cantidad }}</td>
	        <td>$ {{ $pago->descuento }}</td>
	        <td>$ {{$pago->cantidad + $pago->descuento}}</td>
	        <td>            
	            <a href="#" data-id="{{ $pago->id }}" class="btn btn-danger btn-delete glyphicon glyphicon-remove"> </a>
	        </td>
	    </tr>

	    @endforeach
	    </tbody>
	  </table>
</div>
</div>

{{--usado para eliminar usuario --}}
{{ Form::open(array('route' => array('pago.destroy', 'CLIENTE_ID'), 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete')) }}
{{ Form::close() }}
@stop




