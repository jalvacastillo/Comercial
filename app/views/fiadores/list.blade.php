@extends ('index')

@section ('title') Lista de fiadores @stop

@section ('content')

<div class="panel panel-default">
  <div class="panel-heading">
    <p>
        <h1>Lista de fiadores </h1>
        {{ HTML::link('fiadores/create', 'Nuevo', array('class' => 'glyphicon glyphicon-file'))}}
    </p>
  </div>
  <div class="panel-body">
 
    {{ $fiadores->links()}} 

  <table class="table table-striped table-condensed table-hover">
    <tr>
        <th>Codigo</th>
        <th>Nombre</th>
        <th>Telefono</th>
        <th>Direccion</th>
        <th>DUI</th>
        <th>NIT</th>
        <th>Trabajo</th>
        <th>Tel. Trabajo</th>
        <th>Opciones</th>
    </tr>
    @foreach ($fiadores as $fiador)
    <tr>
        <td>{{ $fiador->id }}</td>
        <td>{{ $fiador->nombre }}</td>
        <td>{{ $fiador->telefono}}</td>
        <td>{{ $fiador->direccion}}</td>
        <td>{{ $fiador->dui}}</td>
        <td>{{ $fiador->nit}}</td>
        <td>{{ $fiador->lugarTrabajo}}</td>
        <td>{{ $fiador->telefonoTrabajo}}</td>
        <td>
            <a href="{{ route('fiadores.edit', $fiador->id) }}" class="btn btn-primary glyphicon glyphicon-edit"> </a>
             
            <a href="#" data-id="{{ $fiador->id }}" class="btn btn-danger btn-delete glyphicon glyphicon-remove"> </a>


        </td>
    </tr>

    @endforeach
  </table>
</div>
</div>

{{--usado para eliminar usuario --}}
{{ Form::open(array('route' => array('fiadores.destroy', 'CLIENTE_ID'), 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete')) }}
{{ Form::close() }}
@stop




