@extends ('index')


@section ('title') Nuevo fiador @stop

@section ('content')

<div class="panel panel-default">
<div class="panel-heading">
    <h1>
    	Nuevo fiador  </h1>
        {{ HTML::link('fiadores', 'Todos', array('class' => 'glyphicon glyphicon-list'))}}
        
  		 {{ HTML::link('fiadores/create', 'Nuevo', array('class' => 'glyphicon glyphicon-file'))}}
  </div>
  <div class="panel-body">
	@include ('errors', array('errors' => $errors))

{{ Form::model($fiador, $form_data, array('role' => 'form')) }}
		
	<div class="row">
	    <div class="form-group col-md-1">
	    	
	      {{ Form::label('id', 'Codigo') }}
	      {{ Form::text('id', null, array('placeholder' => 'ID', 'class' => 'form-control', 'readonly'=>'true')) }}
	    </div>
	    <div class="form-group col-md-4">
	      {{ Form::label('nombre', 'Nombre') }}
	      {{ Form::text('nombre', null, array('placeholder' => 'Nombre del fiador', 'class' => 'form-control')) }}
	    </div>
  	</div>
	<div class="row">
	    <div class="form-group col-md-3">
	      {{ Form::label('direccion', 'Direccion') }}
	      {{ Form::text('direccion', null, array( 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group col-md-2">
	      {{ Form::label('telefono', 'Telefono') }}
	      {{ Form::text('telefono', null, array('class' => 'form-control')) }}        
	    </div>
  	</div>
  	<div class="row">
	    <div class="form-group col-md-2">
	      {{ Form::label('nit', 'NIT') }}
	      {{ Form::text('nit', null, array( 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group col-md-2">
	      {{ Form::label('dui', 'DUI') }}
	      {{ Form::text('dui', null, array('class' => 'form-control')) }}        
	    </div>
  	</div>

  	<div class="row">
	    <div class="form-group col-md-3">
	      {{ Form::label('lugarTrabajo', 'Lugar de trabajo') }}
	      {{ Form::text('lugarTrabajo', null, array( 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group col-md-2">
	      {{ Form::label('telefonoTrabajo', 'Telefono de trabajo') }}
	      {{ Form::text('telefonoTrabajo', null, array('class' => 'form-control')) }}        
	    </div>
  	</div>


  	{{ Form::button($action . ' fiador', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
	{{Form::close()}}
	</div>

</div>	
@stop