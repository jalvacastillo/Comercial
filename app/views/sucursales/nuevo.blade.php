@extends ('index')


@section ('title') Nueva Sucursal @stop

@section ('content')

<div class="panel panel-default">
<div class="panel-heading">
    <h1>   	Nueva sucursal  </h1>
        {{ HTML::link('sucursales', 'Todos', array('class' => 'glyphicon glyphicon-list'))}}
        
  		 {{ HTML::link('sucursales/create', 'Nuevo', array('class' => 'glyphicon glyphicon-file'))}}
  </div>
  <div class="panel-body">
	@include ('errors', array('errors' => $errors))

{{ Form::model($sucursal, $form_data, array('role' => 'form')) }}
		
	<div class="row">
	    <div class="form-group col-md-1">
	    	
	      {{ Form::label('id', 'Codigo') }}
	      {{ Form::text('id', null, array( 'class' => 'form-control', 'readonly'=>'true')) }}
	    </div>
	    <div class="form-group col-md-4">
	      {{ Form::label('nombre', 'Nombre') }}
	      {{ Form::text('nombre', null, array('placeholder' => 'Nombre de sucursal', 'class' => 'form-control')) }}
	    </div>
  	</div>
	<div class="row">
	    <div class="form-group col-md-4">
	      {{ Form::label('direccion', 'Direccion') }}
	      {{ Form::text('direccion', null, array( 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group col-md-2">
	      {{ Form::label('telefono', 'Telefono') }}
	      {{ Form::text('telefono', null, array('class' => 'form-control')) }}        
	    </div>
  	</div>
  	<div class="row">
	    <div class="form-group col-md-4">
	      {{ Form::label('notas', 'Notas') }}
	      {{ Form::text('notas', null, array( 'class' => 'form-control')) }}
	    </div>
  	</div>
  	{{ Form::button($action . ' sucursal ', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
	{{Form::close()}}
	</div>

</div>	
@stop