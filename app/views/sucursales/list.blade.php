@extends ('index')

@section ('title') Lista de sucursales @stop

@section ('content')

<div class="panel panel-default">
  <div class="panel-heading">
    <p>
        <h1>Lista de sucursales </h1>
        {{ HTML::link('sucursales/create', 'Nuevo', array('class' => 'glyphicon glyphicon-file'))}}
    </p>
  </div>
  <div class="panel-body">
 
    {{ $sucursales->links()}} 

  <table class="table table-striped table-condensed table-hover">
    <tr>
        <th>Codigo</th>
        <th>Nombre</th>
        <th>Dirección</th>
        <th>Telefono</th>
        <th>Nota</th>
        <th>Opciones</th>
    </tr>
    @foreach ($sucursales as $sucursal)
    
    <tr>
        <td>{{ $sucursal->id }}</td>
        <td>{{ $sucursal->nombre }}</td>
        <td>{{ $sucursal->direccion }}</td>
        <td>{{ $sucursal->telefono}}</td>
        <td>{{ $sucursal->notas}}</td>
        <td>
            <a href="{{ route('sucursales.edit', $sucursal->id) }}" class="btn btn-primary glyphicon glyphicon-edit"> </a>
             
            <a href="#" data-id="{{ $sucursal->id }}" class="btn btn-danger btn-delete glyphicon glyphicon-remove"> </a>


        </td>
    </tr>

    @endforeach
  </table>
</div>
</div>

{{--usado para eliminar usuario --}}
{{ Form::open(array('route' => array('sucursales.destroy', 'CLIENTE_ID'), 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete')) }}
{{ Form::close() }}
@stop




