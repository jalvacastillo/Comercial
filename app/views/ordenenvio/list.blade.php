@extends ('index')

@section ('title') Listado de Ordenes de Envío @stop

@section ('content')

<div class="panel panel-default">
  <div class="panel-heading">
    <p>
        <h1>Ordenes de Envío </h1>
        {{ HTML::link('envio/create', 'Nuevo', array('class' => 'glyphicon glyphicon-file'))}}
    </p>
  </div>
  <div class="panel-body">
 
    {{ $envios->links()}} 

  <table class="table table-striped table-condensed table-hover">
    <thead>
    <tr>
        <th>Codigo</th>
        <th>Sucursal</th>
        <th>Fecha</th>
        <th>Comprobante</th>
        <th>Detalles</th>
        <th>Recibio</th>
        <th>Entrego</th>
        <th>Creado</th>
        <th>Modificado</th>
        <th>Opciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($envios as $envio)
    <tr>
        <td>{{ $envio->id }}</td>
        <td>{{ $envio->sucursal->nombre}}</td>
        <td>{{ $envio->fecha }}</td>
        <td>{{ $envio->comprobante}}</td>
        <td>{{ $envio->detalles}}</td>
        <td>{{ $envio->recibe}}</td>
        <td>{{ $envio->entrega}}</td>
        <td>{{ $envio->created_at}}</td>
        <td>{{ $envio->updated_at}}</td>
        <td>
            <a href="{{ route('envio.edit', $envio->id) }}" class="btn btn-primary glyphicon glyphicon-edit"> </a>
            <a href="{{ route('detalleenvio.edit', $envio->id) }}" class="btn btn-primary glyphicon glyphicon-search"> </a>
             
            <a href="#" data-id="{{ $envio->id }}" class="btn btn-danger btn-delete glyphicon glyphicon-remove"> </a>


        </td>
    </tr>

    @endforeach
    </tbody>
  </table>
</div>
</div>

{{--usado para eliminar --}}
{{ Form::open(array('route' => array('envio.destroy', 'CLIENTE_ID'), 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete')) }}
{{ Form::close() }}
@stop




