@extends ('index')


@section ('title') Nueva Orden Envío @stop

@section ('content')

<div class="panel panel-default">
<div class="panel-heading">
    <h1>
      Nueva Orden Envío  </h1>
        {{ HTML::link('envio', 'Todos', array('class' => 'glyphicon glyphicon-list'))}}
        
       {{ HTML::link('envio/create', 'Nuevo', array('class' => 'glyphicon glyphicon-file'))}}
  </div>
  <div class="panel-body">
  @include ('errors', array('errors' => $errors))

{{ Form::model($ordenenvio, $form_data, array('role' => 'form')) }}
       {{$sucursal = "";}}
  @if($ordenenvio->exists)
    <?php      
      $sucursal = $ordenenvio->sucursal->nombre;
    ?>
  @endif
  <div class="row">
      <div class="form-group col-md-1">
        
        {{ Form::label('id', 'Codigo') }}
        {{ Form::text('id', null, array('placeholder' => 'ID', 'class' => 'form-control', 'readonly'=>'true')) }}
      </div>
      <div class="form-group col-md-3">
        {{ Form::label('comprobante', 'Comprobante') }}
        {{ Form::text('comprobante', null, array('placeholder' => 'Comprobante', 'class' => 'form-control')) }}
      </div>
      <div class="form-group col-md-2">
        {{ Form::label('fecha', 'Fecha') }}
        {{ Form::text('fecha', date("Y-m-d"), array('class' => 'form-control date')) }}        
      </div>
    </div>
  <div class="row">
      <div class="form-group col-md-1">
        {{ Form::label('idSucursal', 'ID prov.') }}
        {{ Form::text('idSucursal', null, array('class' => 'form-control', 'readOnly', 'placeholder' => 'ID sucursal')) }}        
      </div>
      <div class="form-group col-md-3">
        {{ Form::label('search', 'sucursal')}}
        {{ Form::text('search', $sucursal, array( 'class' => 'form-control', 'placeholder' => 'Nombre de sucursal', 'data-url' => 'sucursal', 'data-cod' => 'idSucursal')) }}
      </div>
    </div>
  <div class="row">
      <div class="form-group col-md-3">
        {{ Form::label('recibe', 'Recibe') }}
        {{ Form::text('recibe', null, array( 'class' => 'form-control')) }}     
      </div>
      <div class="form-group col-md-3">
        {{ Form::label('entrega', 'Entrega')}}
        {{ Form::text('entrega', null, array( 'class' => 'form-control')) }}
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-6">
        {{ Form::label('detalles', 'Detalles') }}
        {{ Form::text('detalles', null, array('placeholder' => 'Detalles de la venta', 'class' => 'form-control')) }}
      </div>
      </div>

    {{ Form::button($action . ' Orden Envío', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
  {{Form::close()}}
  </div>

  <script>
    $(function() {
        $( ".date" ).datepicker({
          numberOfMonths: 1,
          dateFormat: "yy-mm-dd"
        });
      });

  </script>
</div>  
@stop