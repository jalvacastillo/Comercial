@extends('inventario.buscar')

@section('movimientos')

    <table class="table table-striped">
  		<thead>
  			<td>Fecha</td>
  			<td>Producto</td>
  			<td>Categoría</td>
  			<td>Cantidad</td>
  			<td>Tipo</td>
  		</thead>
  		<tbody>

  			@foreach($movimientos as $movimiento)
	  			<tr>
	  				<td> {{$movimiento['fecha']}} </td>
	  				<td> {{$movimiento['producto']}} </td>
	  				<td> {{$movimiento['categoria']}} </td>
	  				<td> {{$movimiento['cantidad']}} </td>
	  				<td> {{$movimiento['tipo']}} </td>
	  			</tr>
	  		@endforeach
  		</tbody>
    </table>

@stop