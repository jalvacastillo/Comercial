@extends ('index')

@section ('title') Consulta de entradas y salidas @stop

@section ('content')

<div class="panel panel-default">
<div class="panel-heading">
		{{Form::open()}}
	<div class="row">
	     <div class="form-group col-md-1">
        {{ Form::label('idProducto', 'ID.') }}
        {{ Form::text('idProducto', null, array('class' => 'form-control', 'readOnly', 'placeholder' => 'ID Producto')) }}        
      </div>
      <div class="form-group col-md-2">
        {{ Form::label('search', 'Producto')}}
        {{ Form::text('search', null, array( 'class' => 'form-control', 'placeholder' => 'Nombre de producto', 'data-url' => 'producto', 'data-cod' => 'idProducto')) }}
      </div>
      
	    <div class="form-group col-md-2">
	      {{ Form::label('fechaInicio', 'Fecha Inicio') }}       
	      {{ Form::text('fechaInicio', date("Y-m-d"), array('id' =>'datepicker' ,'class' => 'form-control from')) }} 
	    </div>
	    <div class="form-group col-md-2">
	      {{ Form::label('fechaFin', 'Fecha Fin') }}
	      {{ Form::text('fechaFin', date("Y-m-d"), array( 'class' => 'form-control to')) }}
	    </div>
	    <div class="form-group col-md-1">
  			{{ Form::button('Buscar', array('type' => 'submit', 'class' => 'btn btn-primary pull-left')) }}
	    </div>
  	</div>
  	{{Form::close()}}
	<script>
		$(function() {
		    $( ".from" ).datepicker({
		      defaultDate: "+1w",
		      changeMonth: true,
          numberOfMonths: 1,
		      dateFormat: "yy-mm-dd",
		      onClose: function( selectedDate ) {
		        $( ".to" ).datepicker( "option", "minDate", selectedDate );
		      }
		    });
		    $( ".to" ).datepicker({
		      defaultDate: "+1w",
		      changeMonth: true,
		      numberOfMonths: 1,
		      dateFormat: "yy-mm-dd",
		      onClose: function( selectedDate ) {
		        $( ".from" ).datepicker( "option", "maxDate", selectedDate );
		      }
		    });
		  });

  </script>
  </div>
  <div class="panel-body">

  @yield('movimientos')
  </div>

</div>	
@stop