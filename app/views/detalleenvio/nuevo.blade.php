@extends ('index')


@section ('title') Detalle de Envío @stop

@section ('content')

<div class="panel panel-default">
<div class="panel-heading">
    <h1> Detalle de envío</h1>
        {{ HTML::link('envio', 'Todos', array('class' => 'glyphicon glyphicon-list'))}}
        
       {{ HTML::link('envio/create', 'Nuevo', array('class' => 'glyphicon glyphicon-file'))}}
  </div>
  <div class="panel-body">
  @include ('errors', array('errors' => $errors))

{{ Form::model($ordenenvio, $form_data, array('role' => 'form')) }}
    
        {{ Form::hidden('idOrdenEnvio', $ordenenvio->id, array('class' => 'form-control', 'readOnly', 'placeholder' => 'ID Orden')) }}        
      
  <div class="row">
      <div class="form-group col-md-1">
        {{ Form::label('idProducto', 'ID.') }}
        {{ Form::text('idProducto', null, array('class' => 'form-control', 'readOnly', 'placeholder' => 'ID Producto')) }}        
      </div>
      <div class="form-group col-md-2">
        {{ Form::label('search', 'Producto')}}
        {{ Form::text('search', null, array( 'class' => 'form-control', 'placeholder' => 'Nombre de producto', 'data-url' => 'producto', 'data-cod' => 'idProducto')) }}
      </div>
      <div class="form-group col-md-1">
        {{ Form::label('cantidad', 'Cantidad') }}
        {{ Form::text('cantidad', null, array('class' => 'form-control')) }}        
      </div>
      <div class="form-group col-md-3">
        {{ Form::label('detalles', 'Detalles') }}
        {{ Form::text('detalles', null, array('class' => 'form-control')) }}        
      </div>
    </div>

    {{ Form::button($action . ' envio', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
  {{Form::close()}}
  </div>


<table class="table table-striped table-condensed table-hover">
    <thead>
    <tr>
        <th>Cantidad</th>
        <th>Producto</th>        
        <th>Detalle</th>
        <th>Opciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach (detalleenvio::where('idOrdenEnvio', '=', $ordenenvio->id)->get() as $detalle)
    <tr>
        <td>{{ $detalle->cantidad }}</td>
        <td>{{ $detalle->producto->nombre }}</td> 
        <td> {{$detalle->detalles }} </td>
        <td>
             
            <a href="#" data-id="{{ $detalle->id }}" class="btn btn-danger btn-delete glyphicon glyphicon-remove"> </a>


        </td>
    </tr>

    @endforeach
    </tbody>
  </table>



</div>  
{{--usado para eliminar --}}
{{ Form::open(array('route' => array('detalleenvio.destroy', 'CLIENTE_ID'), 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete')) }}
{{ Form::close() }}
@stop