@extends ('index')

@section ('title') Lista de Clientes @stop

@section ('content')

<div class="panel panel-default">
  <div class="panel-heading">
        <h1> 
            <a href="{{ route('mostrarCliente', $cliente->id)}}">
                {{ $cliente->nombre}} 
            </a>
            <small> {{$cliente->direccion}}  
        <span class="pull-right btn btn-info" id="showForm" > Nuevo </span></small>

    <form action="{{ route('fiadoresCliente', $cliente->id)}}" method="post" id="form" style="display:none" class="pull-right">
        
        <input class="form-control" type="hidden" id="idFiador" name="idFiador" placeholder="ID Proveedor" readonly="readOnly"></input>
       <input id="search" class="form-control" type="text" name="search" data-cod="idFiador" data-url="fiador" placeholder="Nombre de fiador">
        </input>
        <input type="submit" value="Agregar Fiador" class="btn btn-success">
   </form>
        </h1>

    <p>
        Fiadores registrados para el cliente
    </p>  
  </div>
  <div class="panel-body">
  

 <table class="table table-striped table-condensed table-hover">
    <tr>
        <th>Codigo</th>
        <th>Nombre</th>
        <th>Telefono</th>
        <th>Direccion</th>
        <th>DUI</th>
        <th>NIT</th>
        <th>Trabajo</th>
        <th>Tel. Trabajo</th>
        <th>Opciones</th>
    </tr>
    @foreach ($cliente->fiadores as $fiador)
    <tr>
        <td>{{ $fiador->fiador->id }}</td>
        <td>{{ $fiador->fiador->nombre }}</td>
        <td>{{ $fiador->fiador->telefono}}</td>
        <td>{{ $fiador->fiador->direccion}}</td>
        <td>{{ $fiador->fiador->dui}}</td>
        <td>{{ $fiador->fiador->nit}}</td>
        <td>{{ $fiador->fiador->lugarTrabajo}}</td>
        <td>{{ $fiador->fiador->telefonoTrabajo}}</td>
        <td>
            <a href="#" data-id="{{ $fiador->id }}" class="btn btn-danger btn-delete glyphicon glyphicon-remove"> </a>
        </td>
    </tr>

    @endforeach
  </table>
</div>
</div>

<script type="text/javascript">
    $('#showForm').on("click", function(){ $('#form').toggle()});

</script>

{{--usado para eliminar usuario --}}
{{ Form::open(array('route' => array('eliminar-fiador', 'CLIENTE_ID'), 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete')) }}
{{ Form::close() }}
@stop




