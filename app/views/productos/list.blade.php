@extends ('index')

@section ('title') Lista de productos @stop

@section ('content')

<div class="panel panel-default">
  <div class="panel-heading">
    <p>
        <ol class="breadcrumb">
          <li>Inicio</li>
          <li>Productos</li>
        </ol>
        <h1>Lista de productos </h1>
        {{ HTML::link('productos/create', ' Nuevo', array('class' => 'btn btn-default glyphicon glyphicon-file'))}}
    </p>
  </div>
  <div class="panel-body">
 
    {{ $productos->links()}} 
  <table class="table table-striped table-condensed table-hover">
    <tr>
        <th>Codigo</th>
        <th>Producto</th>
        <th>Categoria</th> 
        <th>Unidad</th>
        <th>Minimo</th>
        <th>Cantidad</th>
        <th>Tipo</th>
        <th>Ubicacion</th>
        <th>Modelo</th>
        <th>Garantia</th>
        <th>Tiempo</th>
        <th>Activo</th>
        <th>Opciones</th>
    </tr>
    @foreach ($productos as $producto)
    <tr>
        <td>{{ $producto->id }}</td>
        <td>{{ $producto->nombre }}</td>
        <td>
            <a href="{{ route('productos-categoria', $producto->category->id) }}" > 
                {{ $producto->category->categoria }}
            </a>
        </td>
        <td>{{ $producto->unidad}}</td>
        <td>{{ $producto->minimo}}</td>
        <td class= "success">{{ $producto->compras()->sum('cantidad') - $producto->ventas()->sum('cantidad') - $producto->envios()->sum('cantidad')}}</td>
        
        <td>{{ $producto->tipo}}</td>
        <td>{{ $producto->ubicacion}}</td>
        <td>{{ $producto->modelo}}</td>
        <td>{{ $producto->garantia}}</td>
        <td>{{ $producto->tiempo}}</td>
        <td>{{ $producto->activo ? 'Si' : 'No'}}</td>
        <td>
            <a href="{{ route('productos.edit', $producto->id) }}" class="btn btn-primary glyphicon glyphicon-edit"> </a>
             
            <a href="#" data-id="{{ $producto->id }}" class="btn btn-danger btn-delete glyphicon glyphicon-remove"> </a>


        </td>
    </tr>

    @endforeach
  </table>
</div>
</div>

{{--usado para eliminar usuario --}}
{{ Form::open(array('route' => array('productos.destroy', 'CLIENTE_ID'), 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete')) }}
{{ Form::close() }}
@stop




