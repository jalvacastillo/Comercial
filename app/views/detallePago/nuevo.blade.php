@extends ('index')


@section ('title') Detalle de pago @stop

@section ('content')

<?php 
      
        //Calculos para mostrarse en la tabla.
    $credito = $pago->credito;
   
    $pagos = $credito->pagos()->sum('cantidad');
    $descuento = $credito->pagos()->sum('descuento'); 
    $total = $credito->venta->total(); 
    $saldo = $total  - ($pagos + $credito->prima + $descuento);
    
    //$saldo = 11;
?>


<div class="panel panel-default">
<div class="panel-heading">
    <h3>{{$credito->venta->cliente->nombre}} <small> {{$credito->venta->cliente->direccion}}</small></h3>
  </div>
  <div class="panel-body">
  @include ('errors', array('errors' => $errors))
{{ Form::model($pago, $form_data, array('role' => 'form')) }}
        {{ Form::hidden('idPago', null, array('placeholder' => 'cantidad pagada', 'class' => 'form-control')) }}        
      
  <div class="row">
      <div class="form-group col-md-2">
        {{ Form::label('cantidad', 'Cantidad') }}
        <div class="input-group">
          <span class="input-group-addon">$</span>
          {{ Form::text('cantidad', null, array('placeholder' => 'cantidad pagada', 'class' => 'form-control')) }}        
        </div>
      </div>
      <div class="form-group col-md-2">
        {{ Form::label('descuento', 'Descuento') }}
        <div class="input-group">
          <span class="input-group-addon">$</span>
          {{ Form::text('descuento', null, array('placeholder' => 'Descuento', 'class' => 'form-control')) }}        
        </div>
      </div>
      <div class="form-group col-md-2">
        {{ Form::label('fecha', 'Fecha (año-mes-día)') }}
        {{ Form::text('fecha', date("Y-m-d"), array('class' => 'form-control date')) }}        
      </div>

      <div class="col-md-1">
      <h3><small>Saldo:</small> ${{$saldo}}</h3>
      </div>
      <div class="col-md-4">
        <div class="row">
          <div class="col-md-12">
            <h4>Contrato: {{$credito->venta->contrato}}</h4>
          </div>
        </div>
        <div class="col-md-12">
           <table class="table table-bordered">
            <tr>
            <td>Producto</td>
            <td>Detalle</td>
            </tr>
            @foreach($credito->venta->detalle as $detalle)
              <tr>
                <td>{{$detalle->productos->nombre}}</td>
                <td>{{$detalle->detalle}}</td>
              </tr>
            @endforeach
        </table>
        </div>
      </div>
    </div>

    {{ Form::button($action . ' pago', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
  {{Form::close()}}

  
      
  </div>


<table class="table table-striped table-condensed table-hover">
    <thead>
    <tr>
        <th>Cantidad</th>
        <th>Descuento</th>
        <th>Fecha</th>
        <th>Opciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach (detallePago::where('idPago', '=', $pago->idPago)->get() as $detalle)
    <tr>
        <td>$ {{ $detalle->cantidad }}</td>
        <td>$ {{ $detalle->descuento }}</td>
        <td>{{ $detalle->fecha }}</td>
        <td>
             
            <a href="#" data-id="{{ $detalle->id }}" class="btn btn-danger btn-delete glyphicon glyphicon-remove"> </a>


        </td>
    </tr>

    @endforeach
    </tbody>
  </table>

  <script> 
    $(function() {
        $( ".date" ).datepicker({
          numberOfMonths: 1,
          dateFormat: "yy-mm-dd"
        });
      });

  </script>

</div>  
{{--usado para eliminar usuario --}}
{{ Form::open(array('route' => array('detallepago.destroy', 'CLIENTE_ID'), 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete')) }}
{{ Form::close() }}
@stop