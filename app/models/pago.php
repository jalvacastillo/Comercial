<?php

class Pago extends Eloquent
{
    protected $guarded = array();
    public $errors;

    // public $hola = "hol";
    protected $appends = array('mora', 'saldo');
    protected $fillable = array('idVenta','numeroCuotas','tiempoPago','detallePago','prima');

 
    public function isValid($data)
    {
        $rules = array(
            'idVenta'       => 'required|unique:Pagos',
            'numeroCuotas'  => 'required',
            'detallePago'   => 'required',
            'prima'         => 'required' ,
            'pagoTotal'     => 'required'
        );
        
        if($this->exists())
            $rules['idVenta'] = 'required';

        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }

    public function getMoraAttribute(){
        
        $venta = $this->venta()->first();
        if (!$venta) {
            return false;
        }
        $fechaLimite = date('Y-m-d', strtotime("+$this->numeroCuotas months", strtotime($venta['fecha'])));

        if ($fechaLimite > date('Y-m-d')) {
            return true;
        }else{
            return false;
        }
    }

    public function getSaldoAttribute(){
        
        $pagos = $this->pagos()->sum('cantidad');
        $descuentos = $this->pagos()->sum('descuento');
        $venta = $this->venta()->first();
        return $venta['totale'] - ($pagos + $this->prima + $descuentos);

    }

    public function venta()
    {
        return $this->belongsTo('venta', 'idVenta');
    }

    public function pagos()
    {
        return $this->hasMany('detallePago', 'idPago');
    }

    public function cliente()
    {
        return $this->hasOneThrough('cliente', 'venta', 'idVenta');
    }  


}