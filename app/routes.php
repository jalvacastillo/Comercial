<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::controller('login', 'loginController');

//Route::group(array('before' => 'auth'), function()
//{

	Route::resource('users', 'userController');
	Route::get('/',function(){return View::make('index');});

	Route::get('pago/venta/{idVenta}', ['as' => 'pagar', 'uses' => function($idVenta)
	{
		$pago = pago::where('idVenta', '=', $idVenta)->get()->first(); // Validar si aún no hay hecho un pago para esta venta.
		if(is_null($pago) ){
			//return "no existe este baboso";
			$pago = new pago();
			$pago->prima = 0;
			$form_data = array('route' => 'pago.store', 'method' => 'POST');
			$pago->idVenta = $idVenta;
			//$
	        $action    = 'Continuar';
			return View::make('pagos.nuevo', compact('pago','form_data', 'action'));
		}
			//route('pago.edit', $pago->id)
		//return $pago;
		return Redirect::route('pago.edit', $pago->id);
	}]);

	//Rutas para las categorias
	require (__DIR__ . '/routes/categorias.php');

	//Rutas para las clientes
	require (__DIR__ . '/routes/clientes.php'); 

	//Rutas para las compras
	require (__DIR__ . '/routes/compras.php');





	Route::post('buscar', function()
	{
		$option = Input::get('opciones');
		$term = Input::get('term');

		//$term = 'hola mi amor';

		if($option == 'cliente')
			return Redirect::route('buscarCliente', $term);
		elseif($option == 'proveedor')
			return Redirect::route('buscarProveedor', $term);
		elseif($option == 'producto')
			return Redirect::route('buscarProducto', $term);
		elseif($option == 'categoria')
			return Redirect::route('buscarCategoria', $term);
		elseif($option == 'pagos')
			return Redirect::route('buscarPago', $term);
		elseif($option == 'inventario')
			return Redirect::route('buscarInventario', $term);

		return $option;
		//Redirect::route('clientes');
	//	return "C";
	});

	Route::controller('api','apiController');
	Route::controller('consultas','consultas');

 
//Rutas para las busquedas
	Route::get('proveedores/buscar/{term}', ['as' => 'buscarProveedor', 'uses' => 'proveedoresController@buscar']);
	Route::get('productos/buscar/{term}',   ['as' => 'buscarProducto',  'uses' => 'ProductoController@buscar']);
 	Route::get('categorias/buscar/{term}',  ['as' => 'buscarCategoria', 'uses' => 'categoryController@buscar']);
 	Route::get('pagos/buscar/{term}',       ['as' => 'buscarPago',      'uses' => 'pagoController@buscar']);
 	Route::get('inventario/buscar/{term}',       ['as' => 'buscarInventario',      'uses' => 'inventarioController@buscar']);
 
	//Route::resource('categorias','CategoriaController');
	//Route::resource('clientes','clienteController');
	//Route::resource('compras','comprasController');

 	Route::get('proveedores/compras/{id}', ['as' => 'comprasProveedor', 'uses' => 'proveedoresController@compras']);
 	Route::get('proveedores/productos/{id}', ['as' => 'productosProveedor', 'uses' => 'proveedoresController@productos']);


	Route::resource('detallecompra','detalleCompraController');
	Route::resource('detalleenvio','detalleEnvioController');
	Route::resource('detallepago','detallePagoController');
	Route::resource('detalleventa','detalleVentaController');
	Route::resource('envio','ordenEnvioController');
	Route::resource('fiadores','FiadoresController');
	Route::resource('inventario','InventarioController');
	Route::resource('vendedores','VendedoresController');
	Route::resource('pago', 'pagoController');
	Route::get('pagos/mora', ['as' => 'enMora', 'uses' => 'pagoController@mora']);
	Route::get('pagos/realizados', ['as' => 'pagosRealizados', 'uses' => 'pagoController@pagosRealizados']);

	Route::resource('productos','ProductoController');
	Route::resource('proveedores','ProveedoresController');
	Route::resource('sucursales','sucursalesController');
	Route::resource('ventas','ventaController');

	Route::get('kardex', ['as' => 'kardex', 'uses' => 'ProductoController@kardex']);


	//Inventario
	Route::get('inventario', ['as' => 'inventario', 'uses' => 'inventarioController@index']);
	Route::get('entradas-salidas', ['as' => 'entradaSalida', 'uses' => 'inventarioController@entradaSalida']);
	Route::post('entradas-salidas', ['as' => 'entradaSalidaProducto', 'uses' => 'inventarioController@entradaSalidaProducto']);


	//mostrar detalles de compras
	Route::get('venta/{id}', ['as' => 'productoCliente', 'uses' => 'ventasController@cliente']);
	Route::get('venta/productos/{tabla}/{id}', ['as' => 'producto', 'uses' => 'ventasController@productos']);

//});