 <?php

class detalleEnvioController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		Redirect::to('/');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		Redirect::to('/');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		Redirect::to('/');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		//
		$ordenenvio = ordenenvio::find($id);
		if (is_null ($ordenenvio))
        {
            App::abort(404);
        }
		$form_data = array('route' => array('detalleenvio.update', $ordenenvio->id), 'method' => 'PATCH');
        $action    = 'Guardar';
		//var_dump( $cliente);
		return View::make('detalleenvio/nuevo', compact('ordenenvio', 'form_data', 'action'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		//
		//

		$detalleenvio = new detalleenvio();
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        
        // Revisamos si la data es válido
        if ($detalleenvio->ValidAndSave($data))
        {   
        	//$ordenEnvio = compra::find($id);
        	$detalleenvio = detalleenvio::find($id);
        	return Redirect::route('detalleenvio.edit', $id);
        	//return 'Detalle almacenado correctamente';
        }
        else
        {return Redirect::route('detalleenvio.edit', $id)->withInput()->withErrors($detalleenvio->errors);}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		//
		$detalleenvio = detalleenvio::find($id);
        
        if (is_null ($detalleenvio))
        {
            App::abort(404);
        }
        
        $detalleenvio->delete();

        if (Request::ajax())
        {
            return Response::json(array (
                'success' => true,
                'msg'     => 'Detalle  ' . $detalleenvio->id  .' eliminado',
                'id'      => $detalleenvio->id
            ));
        }
        else
        {
            return Redirect::route('detalleenvio');
        }
	}

}