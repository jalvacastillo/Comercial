 <?php

class detallepagoController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		Redirect::to('/');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		Redirect::to('/');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$detallepago = new detallepago();
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        
        // Revisamos si la data es válido
        if ($detallepago->ValidAndSave($data))
        {   
        	return Redirect::route('detallepago.edit', $detallePago->idPago);
        	//return 'Detalle almacenado correctamente';
        }
        else
        {return Redirect::route('detallepago.edit', $id)->withInput()->withErrors($detallepago->errors);}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		//	$pago = new pago();
		/*$form_data = array('route' => 'pago.store', 'method' => 'POST');
		$pago->idVenta = $idVenta;
        $action    = 'Agregar';
		return View::make('pagos/nuevo', compact('pago','form_data', 'action'));
	});*/
		$pago = new detallePago();
        $pago->idPago = $id;
		$form_data = array('route' => array('detallepago.update', $pago->id), 'method' => 'PATCH');
        $action    = 'asdf';
		//var_dump( $cliente);
		return View::make('detallepago/nuevo', compact('pago', 'form_data', 'action'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		//
		//

		$detallepago = new detallepago();
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        
        // Revisamos si la data es válido
        if ($detallepago->ValidAndSave($data))
        {        
        	return Redirect::route('detallepago.edit', $id);
        	//return 'Detalle almacenado correctamente';
        }
        else
        {return Redirect::route('detallepago.edit', $id)->withInput()->withErrors($detallepago->errors);}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		//
		$detallepago = detallepago::find($id);
        
        if (is_null ($detallepago))
        {
            App::abort(404);
        }
        
        $detallepago->delete();

        if (Request::ajax())
        {
            return Response::json(array (
                'success' => true,
                'msg'     => 'Detalle  ' . $detallepago->id  .' eliminado',
                'id'      => $detallepago->id
            ));
        }
        else
        {
            return Redirect::route('detallepagos');
        }
	}

}