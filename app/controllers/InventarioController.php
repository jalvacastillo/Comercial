<?php

class InventarioController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		//
		$productos = producto::orderBy("nombre", "asc")->paginate();
							
		return View::make('inventario.list', compact('productos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}




	public function buscar($term){
		$productos = producto::orderBy('nombre', 'asc')
							->where('nombre', 'LIKE', '%' . $term . '%') 
							->paginate();
		return View::make('inventario.list', compact('productos'));
	}

//entrada y salida de productos
	public function entradaSalida(){
		$productos = array('' => 'Seleccione un producto') + producto::all()->lists('nombre', 'id');
		return View::make('inventario.buscar', compact('productos'));
	}

	public function entradaSalidaProducto(){
		$id = Input::get('idProducto');
		$fechaInicio = Input::get('fechaInicio');//'2014-01-01';
		$fechaFin 	=  Input::get('fechaFin');//'2014-06-31';

		$compras = compra::with('detalles')
		->whereBetween('fecha', array($fechaInicio, $fechaFin))
		->whereHas('detalles', function($query) use ($id)
		{
		    $query->where('idProducto', $id);
		})
		->get();


		$movimientos = []; //array para almacenar las entradas y salidas de un producto
		foreach ($compras as $compra) {
			foreach ($compra->detalles as $detalle) {
				if($detalle->idProducto == $id)
				{
					$movimientos[] = array(
							'fecha' 	=> $compra->fecha,
							'producto' 	=> $detalle->producto->nombre,
							'categoria' => $detalle->producto->category->categoria,
							'cantidad'	=> $detalle->cantidad,
							'tipo'		=> 'Entrada: Compra'

						);
				}
			}
		}


		$ventas = venta::with('detalle')
		->whereBetween('fecha', array($fechaInicio, $fechaFin))
		->whereHas('detalle', function($query) use ($id)
		{
		    $query->where('idProducto', $id);
		})
		->get();
		foreach ($ventas as $venta) {
			foreach ($venta->detalle as $detalle) {
				if($detalle->idProducto == $id)
				{
					$movimientos[] = array(
							'fecha' 	=> $venta->fecha,
							'producto' 	=> $detalle->productos->nombre,
							'categoria' => $detalle->productos->category->categoria,
							'cantidad'	=> $detalle->cantidad,
							'tipo'		=> 'Salida: Venta'

						);
				}
			}
		}

		//return $movimientos;
		$ordenes = ordenenvio::whereHas('detalles', function($query) use ($id)
		{
		    $query->where('idProducto', $id);
		})
		->whereBetween('fecha', array($fechaInicio, $fechaFin))
		->get();
		// return $ordenes;
		foreach ($ordenes as $orden) {
			foreach (detalleenvio::where('idOrdenEnvio', '=', $orden->id)->get() as $detalle)
			// foreach ($orden->detalles as $detalle) {
				if($detalle->idProducto == $id)
				{
					$movimientos[] = array(
							'fecha' 	=> $orden->fecha,
							'producto' 	=> $detalle->producto->nombre,
							'categoria' => $detalle->producto->category->categoria,
							'cantidad'	=> $detalle->cantidad,
							'tipo'		=> 'Salida: Orden Envio'

						);
				}
			// }
			// return 'Detalles ' . $orden['detalles'];//->detalles;//->detalles;
		}

		//return $ordenes;
		// ->all();
		usort($movimientos, function($a1, $a2) {
			   $v1 = strtotime($a1['fecha']);
			   $v2 = strtotime($a2['fecha']);
			   return $v1 - $v2; // $v2 - $v1 to reverse direction
			});
		

		// return $movimientos;

		$productos = array('' => 'Seleccione un producto') + producto::all()->lists('nombre', 'id');
		return View::make('inventario.movimientos', compact('movimientos', 'productos'));
		// return View::make('pagos.list')->with('pagos', $pagos);
	}



}