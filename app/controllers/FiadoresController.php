<?php

class FiadoresController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$fiadores = fiador::orderBy('id','desc')->paginate(12);
		return View::make('fiadores.list')->with('fiadores', $fiadores);
	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$fiador = new fiador();
		$form_data = array('route' => 'fiadores.store', 'method' => 'POST');
        $action    = 'Crear';
		return View::make('fiadores/nuevo', compact('fiador','form_data', 'action'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$fiador = new fiador;
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        
        // Revisamos si la data es válido
        if ($fiador->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $fiador->fill($data);
            // Guardamos el usuario
            $fiador->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::to('fiadores');
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
			return Redirect::route('fiadores.create')->withInput()->withErrors($fiador->errors);
        }



	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$fiador = fiador::find($id);
		if (is_null ($fiador))
        {
            App::abort(404);
        }
		$form_data = array('route' => array('fiadores.update', $fiador->id), 'method' => 'PATCH');
        $action    = 'Editar';
		//var_dump( $fiador);
		return View::make('fiadores/nuevo', compact('fiador', 'form_data', 'action'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		// Creamos un nuevo objeto para nuestro nuevo usuario
        $fiador = fiador::find($id);
        
        // Si el usuario no existe entonces lanzamos un error 404 :(
        if (is_null ($fiador))
        {
            App::abort(404);
        }
        
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        
        // Revisamos si la data es válido
        if ($fiador->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $fiador->fill($data);
            // Guardamos el usuario
            $fiador->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::to('fiadores');
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::route('fiadores.edit', $fiador->id)->withInput()->withErrors($fiador->errors);
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$fiador = fiador::find($id);
        
        if (is_null ($fiador))
        {
            App::abort(404);
        }
        
        $fiador->delete();

        if (Request::ajax())
        {
            return Response::json(array (
                'success' => true,
                'msg'     => 'Usuario ' . $fiador->nombre . ' ' . $fiador->apellidos .' eliminado',
                'id'      => $fiador->id
            ));
        }
        else
        {
            return Redirect::route('fiadores');
        }




	}


}