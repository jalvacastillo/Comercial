<?php

class sucursalesController extends \BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$sucursales = sucursal::paginate(12);
		return View::make('sucursales.list')->with('sucursales',$sucursales);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$sucursal = new sucursal();
		$form_data = array('route' => 'sucursales.store', 'method' => 'POST');
        $action    = 'Crear';
		return View::make('sucursales/nuevo', compact('sucursal','form_data', 'action'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$sucursal = new sucursal();
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        
        // Revisamos si la data es válido
        if ($sucursal->ValidAndSave($data))
        {   return Redirect::to('sucursales');}
        else
        {return Redirect::route('sucursales.create')->withInput()->withErrors($sucursal->errors);}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		//
		$sucursal = sucursal::find($id);
		if (is_null ($sucursal))
        {
            App::abort(404);
        }
		$form_data = array('route' => array('sucursales.update', $sucursal->id), 'method' => 'PATCH');
        $action    = 'Editar';
		//var_dump( $cliente);
		return View::make('sucursales/nuevo', compact('sucursal', 'form_data', 'action'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		//
		// Creamos un nuevo objeto para nuestro nuevo usuario
        $sucursal = sucursal::find($id);
        
        // Si el usuario no existe entonces lanzamos un error 404 :(
        if (is_null ($sucursal))
        {
            App::abort(404);
        }
        
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        
        // Revisamos si la data es válido
        if ($sucursal->ValidAndSave($data))
        {
            return Redirect::to('sucursales');
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::route('sucursales.edit', $sucursal->id)->withInput()->withErrors($sucursal->errors);
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		//
		$sucursal = sucursal::find($id);
        
        if (is_null ($sucursal))
        {
            App::abort(404);
        }
        
        $sucursal->delete();

        if (Request::ajax())
        {
            return Response::json(array (
                'success' => true,
                'msg'     => 'Usuario ' . $sucursal->nombre  .' eliminado',
                'id'      => $sucursal->id
            ));
        }
        else
        {
            return Redirect::route('sucursales');
        }
	}

}