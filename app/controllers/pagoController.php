<?php

class pagoController extends BaseController
{



	public function index()
	{
		//$pagos = pago::paginate();
		$pagos = pago::with('venta.cliente', 'venta.detalle')
					->paginate();

		// $pagos = pago::where('mora', '=', 'true')
		// 				->paginate();

		//$detallepago = detallePago::where('idVenta', '=','')
		return View::make('pagos.list')->with('pagos', $pagos);
	}

	public function create()
	{
		//
		$pago = new pago();
		$form_data = array('route' => 'pago.store', 'method' => 'POST');
        $action    = 'Continuar';
		return View::make('pagos.nuevo', compact('pago','form_data', 'action'));
	}

	public function store()
	{
		$pago = new pago;
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        
        // Revisamos si la data es válido
        if ($pago->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $pago->fill($data);
		//
			$pago2 = pago::find($pago->idPago);

			//return $
			if (!is_null ($pago2))
        	{
				$form_data = array('route' => array('pago.update', $pago->id), 'method' => 'PATCH');
		        $action    = 'Editar';
				//var_dump( $pago);
				return View::make('pagos.nuevo', compact('pago', 'form_data', 'action'));
	         
	        }
	        $pago->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::route('detallepago.edit', $pago->id);
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
			return Redirect::route('pagar', $data['idVenta'])->withInput()->withErrors($pago->errors);
        }


	}



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$pago = pago::find($id);
		if (is_null ($pago))
        {
            App::abort(404);
        }
		//var_dump( $pago);
		return View::make('pago.detail')->with('pago', $pago);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$pago = pago::find($id);
		if (is_null ($pago))
        {
        	$pago = new pago();
        	$pago->idVenta = $id;
        }
		$form_data = array('route' => array('pago.update', $pago->id), 'method' => 'PATCH');
        $action    = 'Editar';
		//var_dump( $pago);
		return View::make('pagos.nuevo', compact('pago', 'form_data', 'action'));
	}






	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $pago = pago::find($id);
        
        if (is_null ($pago))
        {
         	return Redirect::to('pago');
        }
        
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        
        // Revisamos si la data es válido
        if ($pago->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $pago->fill($data);
            // Guardamos el usuario
            $pago->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::to('pago');
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::route('pago.edit', $pago->id)->withInput()->withErrors($pago->errors);
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$pago = pago::find($id);
        
        if (is_null ($pago))
        {
            App::abort(404);
        }
        
        $pago->delete();

        if (Request::ajax())
        {
            return Response::json(array (
                'success' => true,
                'msg'     => 'Usuario ' . $pago->nombre . ' ' . $pago->apellidos .' eliminado',
                'id'      => $pago->id
            ));
        }
        else
        {
            return Redirect::route('pago');
        }
	}

	public function mora(){
		$pagos = pago::with('venta.cliente', 'venta.detalle')->orderBy('created_at', 'desc')->paginate(100000000);

		$pagos = $pagos->filter(function($pago)
		{
			
		    return ($pago->mora == true) && ($pago->saldo > 0 );

		})->values();

		// return $pagos;
		return View::make('pagos.list', compact('pagos'));
	}


	public function pagosRealizados(){
		$pagos = detallePago::with('credito', 'credito.venta', 'credito.venta.cliente')
							->orderBy('fecha', 'DESC')
							->paginate(20);
		return View::make('pagos/listaPagos', compact('pagos'));
	}

	public function buscar($term){
		$pagos = pago::whereHas('venta', function($query) use ($term)
		{
		    $query->whereHas('cliente', function($query) use ($term)
		    {
		        $query->where('nombre', 'LIKE', '%' . $term . '%');
		    });
		})
		->orderBy('id', 'desc')
		->paginate(20);
		//return $pagos;
		return View::make('pagos.list')->with('pagos', $pagos);
	}

}