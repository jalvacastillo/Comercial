<?php

class ordenEnvioController extends \BaseController {

	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$envios = ordenenvio::orderBy('id', 'DESC')
							->paginate(12);
		return View::make('ordenenvio.list')->with('envios', $envios);
	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$ordenenvio = new ordenenvio();
		$form_data = array('route' => 'envio.store', 'method' => 'POST');
        $action    = 'Crear';
		return View::make('ordenenvio/nuevo', compact('ordenenvio','form_data', 'action'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$ordenenvio = new ordenenvio;
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        
        // Revisamos si la data es válido
        if ($ordenenvio->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $ordenenvio->fill($data);
            // Guardamos el usuario
            $ordenenvio->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::route('detalleenvio.edit', $ordenenvio->id);
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
			return Redirect::route('envio.create')->withInput()->withErrors($ordenenvio->errors);
        }



	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$ordenenvio = ordenenvio::find($id);
		if (is_null ($ordenenvio))
        {
            App::abort(404);
        }
		//var_dump( $ordenenvio);
		return View::make('envio.detail')->with('ordenenvio', $ordenenvio);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$ordenenvio = ordenenvio::find($id);
		if (is_null ($ordenenvio))
        {
            App::abort(404);
        }
		$form_data = array('route' => array('envio.update', $ordenenvio->id), 'method' => 'PATCH');
        $action    = 'Editar';
		//var_dump( $ordenenvio);
		return View::make('ordenenvio/nuevo', compact('ordenenvio', 'form_data', 'action'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		// Creamos un nuevo objeto para nuestro nuevo usuario
        $ordenenvio = ordenenvio::find($id);
        
        // Si el usuario no existe entonces lanzamos un error 404 :(
        if (is_null ($ordenenvio))
        {
            App::abort(404);
        }
        
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        
        // Revisamos si la data es válido
        if ($ordenenvio->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $ordenenvio->fill($data);
            // Guardamos el usuario
            $ordenenvio->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::to('envio');
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::route('envio.edit', $ordenenvio->id)->withInput()->withErrors($ordenenvio->errors);
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$ordenenvio = ordenenvio::find($id);
        
        if (is_null ($ordenenvio))
        {
            App::abort(404);
        }
        
        $ordenenvio->delete();

        if (Request::ajax())
        {
            return Response::json(array (
                'success' => true,
                'msg'     => 'Usuario ' . $ordenenvio->nombre . ' ' . $ordenenvio->apellidos .' eliminado',
                'id'      => $ordenenvio->id
            ));
        }
        else
        {
            return Redirect::route('envio');
        }




	}
}