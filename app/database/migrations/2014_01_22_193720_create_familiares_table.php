<?php

use Illuminate\Database\Migrations\Migration;

class CreateFamiliaresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('familiares', function($table){
			$table->increments('id');
            $table->string('nombre');
            $table->string('direccion');
            $table->string('telefono');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('familiares');
	}

}