<?php

use Illuminate\Database\Migrations\Migration;

class CreateDatellePagoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('detallePago', function($table)
		{
			$table->increments('id');
            $table->integer('idPago');
            $table->decimal('cantidad', 10, 2);
            $table->decimal('descuento', 10, 2);
            $table->datetime('fecha');

            $table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	 	//
	 	Schema::drop('detallePago');
	}

}