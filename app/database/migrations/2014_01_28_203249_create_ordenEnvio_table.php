<?php

use Illuminate\Database\Migrations\Migration;

class CreateOrdenEnvioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ordenenvio', function($table)
		{
			$table->increments('id');
            $table->integer('idSucursal');
            $table->dateTime('fecha');
            $table->string('comprobante');
            $table->string('recibe');
            $table->string('entrega');
            $table->string('detalles');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('ordenenvio');
	}

}