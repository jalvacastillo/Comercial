<?php

use Illuminate\Database\Migrations\Migration;

class CreatePagoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('pagos', function($table){
			$table->increments('id');
            $table->integer('idVenta');
            $table->integer('numeroCuotas');            
            $table->integer('tiempoPago');            
            $table->string('detallePago'); //mes-año
            $table->decimal('prima', 10, 2);
            $table->decimal('pagoTotal', 10, 2); //cantidad a pagar.

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Scheme::drop('pagos');
	}

}