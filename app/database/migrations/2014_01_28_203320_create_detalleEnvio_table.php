<?php

use Illuminate\Database\Migrations\Migration;

class CreateDetalleEnvioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detallenvio', function($table){
			$table->increments('id');
            $table->integer('idOrdenEnvio');
            $table->string('idProducto');
            $table->integer('cantidad');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('detalleenvio');
	}


}