<?php

use Illuminate\Database\Migrations\Migration;

class FiadoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('fiadores', function($table){
			$table->increments('id');
            $table->string('nombre');
            $table->string('direccion');
            $table->string('telefono');
            $table->string('dui');
            $table->string('nit');
            $table->string('lugarTrabajo');
            $table->string('telefonoTrabajo');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('fiadores');
	}

}